﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class formCalculadora : Form
    {
        float primero;
        float segundo;
        float resultado;
        string operacion;

        public formCalculadora()
        {
            InitializeComponent();
        }

        private void btnPunto_Click(object sender, EventArgs e)
        {
          
                    
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "0";
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text + "9";
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtResultado.Clear();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnDividir_MouseEnter(object sender, EventArgs e)
        {
            btnDividir.BackColor = Color.FromArgb(74, 74, 74);
        }

        private void btnDividir_MouseLeave(object sender, EventArgs e)
        {
            btnDividir.BackColor = Color.FromArgb(19, 19, 19);
        }

        private void btnLimpiar_MouseEnter(object sender, EventArgs e)
        {
            btnLimpiar.BackColor = Color.FromArgb(74, 74, 74);
        }

        private void btnLimpiar_MouseLeave(object sender, EventArgs e)
        {
            btnLimpiar.BackColor = Color.FromArgb(19, 19, 19);
        }

        private void btnMultiplicar_MouseEnter(object sender, EventArgs e)
        {
            btnMultiplicar.BackColor = Color.FromArgb(74, 74, 74);
        }

        private void btnMultiplicar_MouseLeave(object sender, EventArgs e)
        {
            btnMultiplicar.BackColor = Color.FromArgb(19, 19, 19);
        }

        private void btnBorrar_MouseEnter(object sender, EventArgs e)
        {
            btnBorrar.BackColor = Color.FromArgb(74, 74, 74);
        }

        private void btnBorrar_MouseLeave(object sender, EventArgs e)
        {
            btnBorrar.BackColor = Color.FromArgb(19, 19, 19);
        }

        private void btnMenos_MouseEnter(object sender, EventArgs e)
        {
            btnMenos.BackColor = Color.FromArgb(74, 74, 74);
        }

        private void btnMenos_MouseLeave(object sender, EventArgs e)
        {
            btnMenos.BackColor = Color.FromArgb(19, 19, 19);
        }

        private void btnMas_MouseEnter(object sender, EventArgs e)
        {
            btnMas.BackColor = Color.FromArgb(74, 74, 74);
        }

        private void btnMas_MouseLeave(object sender, EventArgs e)
        {
            btnMas.BackColor = Color.FromArgb(19, 19, 19);
        }

        private void btnIgual_MouseEnter(object sender, EventArgs e)
        {
            btnIgual.BackColor = Color.FromArgb(74, 74, 74);
        }

        private void btnIgual_MouseLeave(object sender, EventArgs e)
        {
            btnIgual.BackColor = Color.FromArgb(19, 19, 19);
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            txtResultado.Text = txtResultado.Text.Substring(0, txtResultado.Text.Count() - 1);
        }

        private void btnMas_Click(object sender, EventArgs e)
        {
            operacion = "+";
            primero = float.Parse(txtResultado.Text);
            txtResultado.Clear();
        }

        private void btnMenos_Click(object sender, EventArgs e)
        {
            operacion = "-";
            primero = float.Parse(txtResultado.Text);
            txtResultado.Clear();
        }

        private void btnMultiplicar_Click(object sender, EventArgs e)
        {
            operacion = "*";
            primero = float.Parse(txtResultado.Text);
            txtResultado.Clear();
        }

        private void btnDividir_Click(object sender, EventArgs e)
        {
            operacion = "/";
            primero = float.Parse(txtResultado.Text);
            txtResultado.Clear();
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {
            segundo = float.Parse(txtResultado.Text);

            switch(operacion){
                case "+":

                    resultado = primero + segundo;
                    txtResultado.Text = resultado.ToString();

                    break;
                case "-":

                    resultado = primero - segundo;
                    txtResultado.Text = resultado.ToString();

                    break;
                case "*":

                    resultado = primero * segundo;
                    txtResultado.Text = resultado.ToString();

                    break;
                case "/":

                    resultado = primero / segundo;
                    txtResultado.Text = resultado.ToString();

                    break;


            }

        }
    }
}
