﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prueba
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSaludo_Click(object sender, EventArgs e)
        {
            labelMensaje.Text = "Hola a Todos";
        }

        private void btnDespedida_Click(object sender, EventArgs e)
        {
            labelMensaje.Text = "Adios a Todos";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            labelMensaje.Text = "";
        }
    }
}
