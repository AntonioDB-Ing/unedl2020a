﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReloChecador
{
    public partial class Form1 : Form
    {
        DateTime datoEntrada = new DateTime();
        DateTime tiempo = new DateTime();
        DateTime datoSalida = new DateTime();
        string f;
        int bandera = 0;

        public string Archivo = @"C:\Users\diego\Documents\Registro de Empleados\Entrada y Salida.txt";

        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            f = boxEmpleado.Text;

            if (!File.Exists(Archivo))
            {
                using (StreamWriter sw = File.CreateText(Archivo))
                {
                    sw.WriteLine(f);
                    sw.WriteLine(datoEntrada);
                    sw.WriteLine(datoSalida);

                }
            }
            else
            {
                string append = "";
                if (bandera == 1)
                {
                    append = "Entrada:\n" + f + "\n" + datoEntrada + Environment.NewLine;
                }
                else if (bandera == 2)
                {
                    append = "Salida:\n" + f + "\n" + datoSalida + Environment.NewLine;
                }

                File.AppendAllText(Archivo, append);
                datoEntrada = tiempo;

            }

        }

        private void btnrevisar_Click(object sender, EventArgs e){
            string readText = File.ReadAllText(Archivo);
            MessageBox.Show(readText);

        }

        private void rtbnEntrada_CheckedChanged(object sender, EventArgs e)
        {
            datoEntrada = dateTimePicker.Value;
            btnregistrar.Enabled = true;
            btnrevisar.Enabled = true;
            bandera = 1;
        }

        private void rbtnSalida_CheckedChanged(object sender, EventArgs e)
        {
            datoSalida = dateTimePicker.Value;
            btnregistrar.Enabled = true;
            btnrevisar.Enabled = true;
            bandera = 2;
        }

        private void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            datoEntrada = dateTimePicker.Value;
        }

    }

 }


