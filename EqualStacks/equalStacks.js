'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(str => str.trim());

    main();
});

function readLine() {
    return inputString[currentLine++];
}
function suma(arr){
    return arr.reduce((p,c)=>p+c);
}
function equalStacks(h1, h2, h3) {

    if(h1.length === 0 || h2.length === 0  || h3.length === 0) return 0;

    // Se obtiene la suma para cada matriz 
     const pilas =[
        {arr : h1, total : suma(h1)},
        {arr : h2, total : suma(h2)},
        {arr : h3, total : suma(h3)},
    ]

    if(pilas[0].total === pilas[1].total && pilas[0].total === pilas[2].total) 
    return pilas[0].total;

    while(true){
        let masAlta = pilas[0];
        for(let i = 1; i < pilas.length; i++){
            const pila = pilas[i];
            if(masAlta.total < pila.total) masAlta = pila;
        }
    
    masAlta.total -= masAlta.arr.shift();
    if(pilas[0].total === pilas[1].total && pilas[0].total === pilas[2].total)
    break;    
    }
    return pilas[0].total;

}

function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const n1N2N3 = readLine().split(' ');

    const n1 = parseInt(n1N2N3[0], 10);

    const n2 = parseInt(n1N2N3[1], 10);

    const n3 = parseInt(n1N2N3[2], 10);

    const h1 = readLine().split(' ').map(h1Temp => parseInt(h1Temp, 10));

    const h2 = readLine().split(' ').map(h2Temp => parseInt(h2Temp, 10));

    const h3 = readLine().split(' ').map(h3Temp => parseInt(h3Temp, 10));

    let result = equalStacks(h1, h2, h3);

    ws.write(result + "\n");

    ws.end();
}